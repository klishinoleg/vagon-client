<?php
$not_auth = true;
require_once __DIR__ . "/../../loader.php";
if (!empty($car = Functions::array_value($_GET, 'car')))
    if (!empty($data = Functions::getDislocation($car)))
        echo json_encode($data);
    else
        echo 'Нет данных. Повторите запрос позже.';
else
    echo 'Не указан номер вагона';