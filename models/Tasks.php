<?php
/**
 * Created by PhpStorm.
 * User: klish
 * Date: 06.09.2019
 * Time: 22:42
 */

namespace Models;


use DB\dbObject;

/**
 * Class Tasks
 * @package Models
 * @property int $createUserId
 * @property string $createdAt
 * @property int $removeUserId
 * @property string $removeAt
 * @property int $territory_id
 * @property string $car
 */
class Tasks extends dbObject
{

    protected $dbTable = VAGON_PREFIX . 'tasks';
    protected $dbFields = Array (
        'createUserId' => Array ('int'),
        'createdAt' => Array ('datetime'),
        'removeUserId' => Array ('int'),
        'removeAt' => Array ('datetime'),
        'territory_id' => Array ('int'),
        'car' => Array ('text')
    );

    protected $timestamps = Array ('createdAt', 'removeAt');

    protected $relations = Array (
        'createUserId' => Array ("hasOne", "User"),
        'removeUserId' => Array ("hasOne", "User"),
        'userCreate' => Array ("hasOne", "User", "createUserId"),
        'userRemove' => Array ("hasOne", "User", "removeUserId")

    );

}