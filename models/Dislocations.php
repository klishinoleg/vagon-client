<?php
namespace Models;

use DB\dbObject;

/**
 * Class Dislocations
 * @package Models
 * @property string $createdAt
 * @property int $serviceId
 * @property string $oper_station
 * @property string $oper_station_name
 * @property string $oper_code
 * @property string $oper_date
 * @property int $train_num
 * @property string $dest_station
 * @property string $dest_station_name
 * @property string $car
 * @property int $rest_distance
 * @property string $loading_date
 *
 */
class Dislocations extends dbObject
{
    protected $dbTable =  VAGON_PREFIX . 'dislocations';
    protected $dbFields = [
        'createdAt' => Array ('datetime'),
        'serviceId' => Array ('int'),
        'oper_station' => Array ('text'),
        'oper_station_name' => Array ('text'),
        'oper_code' => Array ('text'),
        'oper_date' => Array ('datetime'),
        'train_num' => Array ('int'),
        'dest_station' => Array ('text'),
        "dest_station_name" => Array ('text'),
        "car" => ['text'],
        "rest_distance" => ['int'],
        "loading_date" => ['text'],
        "oper_full_name" => ['text']

    ];
    protected $timestamps = Array ('createdAt');
}