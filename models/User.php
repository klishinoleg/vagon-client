<?php
/**
 * Created by PhpStorm.
 * User: klish
 * Date: 06.09.2019
 * Time: 10:52
 */

namespace Models;


use DB\dbObject;
/**
 * To make IDEs autocomplete happy
 *
 * @property int id
 * @property string login
 * @property bool active
 * @property string customerId
 * @property string firstName
 * @property string lastName
 * @property string password
 * @property string createdAt
 * @property string updatedAt
 * @property string expires
 * @property string PHPSESSID
 * @property int loginCount
 */

class User extends dbObject
{
    /**
     * @var string
     */
    protected $dbTable = VAGON_PREFIX . 'users';

    /**
     * @var array
     */
    protected $dbFields = Array (
        'login' => Array ('text', 'required'),
        'active' => Array ('bool'),
        'customerId' => Array ('int'),
        'firstName' => Array ('/[a-zA-Zа-яА-Я0-9 ]+/'),
        'lastName' => Array ('text'),
        'password' => Array ('text'),
        'createdAt' => Array ('datetime'),
        'updatedAt' => Array ('datetime'),
        'expires' => Array ('datetime'),
        'loginCount' => Array ('int'),
        'PHPSESSID' => Array('text')
    );
    protected $timestamps = Array ('registeredAt', 'updatedAt');
    protected $relations = Array (
        'createdTasks' => Array ("hasMany", "Task", 'createUserId'),
        'removedTasks' => Array ("hasMany", "Task", 'removeUserId')
    );

}