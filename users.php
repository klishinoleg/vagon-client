<?php
session_start();
require_once __DIR__ . "/loader.php";
if (Functions::array_value($_POST, 'add_user') === 'Y')

    Functions::createUser(
        Functions::array_value($_POST, 'login'),
        Functions::array_value($_POST, 'password'),
        Functions::array_value($_POST, 'firstName'),
        Functions::array_value($_POST, 'lastName')
    );

elseif (($remove_id = intval(Functions::array_value($_GET, 'remove'))) > 0)
    Functions::deleteUser($remove_id);
    require __DIR__ . "/view/view_users.php";