<?php
/**
 * Created by PhpStorm.
 * User: klish
 * Date: 06.09.2019
 * Time: 21:24
 */
namespace Api;

class Vagon
{

    const BASE_URL = "https://vagon.online/api/v1/dislocations";

    const TERRITORIES = [
        20=>"Россия",
        21=>"Белоруссия",
        22=>"Украина",
        24=>"Литва",
        25=>"Латвия",
        26=>"Эстония",
        27=>"Казахстан",
        29=>"Узбекистан",
        31=>"Монголия",
        57=>"Азербайджан",
        59=>"Киргизстан",
        66=>"Таджикистан",
        67=>"Туркменистан"
    ];

    /**
     * @param int $territory_id
     * @param array $cars
     * @return bool|array
     *
     * {
    * "status": "OK",
    * "cars": {
    * "29429693": "Дислокация вагона уже запрошена. Дождитесь обновления данных",
    * "53704003": "Дислокация запрошена успешно",
    * "53706420": "Дислокация вагона уже запрошена. Дождитесь обновления данных"
    },
    "added_cars": [
    "53704003"
    ]
    }
     *
     */
    public static function set($territory_id=0, $cars=[]){
        return static::getUrlQuery()->json([
            'territory_id' => (string)$territory_id,
            'cars' => self::toStringCars($cars)
        ]);
    }

    /**
     * @param array $cars
     * @return bool|mixed
     */
    public static function remove($cars=[]){
        return static::getUrlQuery('/remove')->json(
            ['cars' => self::toStringCars($cars)]
        );
    }

    /**
     * @param array $cars
     * @return array
     */
    private static function toStringCars ($cars=[]) {
        foreach ($cars as &$car)
            $car = (string)$car;
        return $cars;
    }

    /**
     * @return bool|mixed
     */
    public static function get(){
        return static::getUrlQuery()->get();
    }

    /**
     * @param string $dir
     * @return \Url
     */
    private static function getUrlQuery($dir=''){

        return new \Url(
            self::BASE_URL . $dir,
            [
                'user' => VAGON_LOGIN,
                'token' => VAGON_TOKEN
            ]);

    }

}