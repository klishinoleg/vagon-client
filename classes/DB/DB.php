<?php
/**
 * Created by PhpStorm.
 * User: klish
 * Date: 06.09.2019
 * Time: 20:28
 */

namespace DB;


class DB extends MysqliDb
{

    public function __construct()
    {
        parent::__construct(DB_HOST, DB_NAME, DB_PASSWORD, DB_NAME);
    }

}