<?php
session_start();
/**
 * Created by PhpStorm.
 * User: klish
 * Date: 08.09.2019
 * Time: 19:24
 */


class Functions
{
    /**
     * @var array
     */
    private static $messages = [];

    public static function showMessagesLikeString(){
        $str = '';
        foreach (self::$messages as $message)
            $str .= '<span class="'.(($message['status'])?'green':'red').'">'.$message['msg'].'</span><br />';
        return $str;

    }

    public static function checkHaveUsers () {

        $User_model = new \Models\User();
        return is_null($User_model->getOne());
    }

    /**
     * @var \Models\User
     */
    private static $User = false;

    static private function showStatusMessage($msg='', $status=false){
        if (empty($msg))
            return true;
        array_push(self::$messages, ['msg' => $msg, 'status'=>$status]);
        return $status;
    }

    /**
     * @return bool|null
     */
    static function Autorisation() {

        $User_model = new \Models\User();
        if (isset($_COOKIE['PHPSESSID']))
            static::$User = $User_model->where('PHPSESSID', $_COOKIE['PHPSESSID'])->getOne();

        if (!empty(static::$User))
            return static::showStatusMessage();

        if (!isset($_POST['name_main']))
            return self::showStatusMessage('No data form');

        try {
                static::$User = $User_model->where('login', $_POST['name_main'])->where('password',sha1($_POST['password_main']))->getOne();

            if (empty(static::$User))
                return self::showStatusMessage('Incorrect login or password');

                static::$User->PHPSESSID = $_COOKIE['PHPSESSID'];
                static::$User->save();

                $_SESSION['AUTH'] = true;
            }
            catch (Exception $exception){
                var_dump($exception);
            }
        return self::showStatusMessage();
    }

    public static  function redirect($location='/') {

        header("Location:{$location}",TRUE,302);
        exit();

    }

    /**
     *
     */
    public static function userExit() {
            session_start();
            session_unset();
            session_destroy();
            unset($_POST['name_main']);
            unset($_POST['password_main']);
            static::$User->PHPSESSID = '';
            static::$User->save();

            Setcookie(session_name(),"");
            Setcookie("SESSID","");
    }

    /**
     * @param array $arr
     * @param string $index
     * @return bool|mixed
     */
    public static function array_value($arr=[], $index=''){
        if (!isset($arr[$index]))
            return false;
        return $arr[$index];
    }

    /**
     * @return \Models\User
     */
    public static function getUser()
    {
        return self::$User;
    }

    /**
     * @param int $territory_id
     * @param string $cars_str
     * @return bool|array
     */

    public static function addDislocation ($territory_id=0, $cars_str='') {
        if(false === ($cars_str = self::validFormLength($cars_str)))
            return false;
        $cars = explode(',', $cars_str);
        /*foreach ($cars as &$car)
            $car = self::clearNumbers($car);*/
        $territory_id = self::clearNumbers($territory_id);
        $res = \Api\Vagon::set($territory_id, $cars);
        self::addTasks($res, $territory_id);
        return self::array_value($res, 'cars');
    }

    /**
     * @param string $car
     * @return bool|mixed
     */
    public static function removeDislocations ($car=''){
        $res = \Api\Vagon::remove([$car]);
        self::removeTasks($res);
        return self::array_value($res, 'removed_cars');
    }

    /**
     * @param $res array
     * @return bool
     */
    public static function removeTasks($res){
        if (empty($res) || empty($removed_cars = self::array_value($res, 'removed_cars')))
            return false;
        foreach ($removed_cars as $removed_car){
            $Task_model = new \Models\Tasks();
            /**
             * @var $Task \Models\Tasks
             */
            $Task = $Task_model->where('removeUserId', NULL, 'IS')->where('car', $removed_car)->getOne();
            if (!empty($Task)) {
                $Task->removeAt = (new DB\DB())->now();
                $Task->removeUserId = static::$User->id;
                $Task->save();
            }
        }
        return true;
    }

    /**
     * @param int $car
     * @return array|null
     */
    public static function getDislocation($car=0) {
        self::updateDislocations();
        $Dislocation_model = new \Models\Dislocations();
        return $Dislocation_model->where('car', $car)->orderBy('createdAt', 'desc')->ArrayBuilder()->getOne();

    }

    private static function updateDislocations() {
        $res = \Api\Vagon::get();
        if (!empty($dislocations = self::array_value($res, 'dislocation')))
            foreach ($dislocations as $dislocation){
                $Dislocation_model = new \Models\Dislocations(
                    [
                        'car' => self::array_value($dislocation, 'vagon_number'),
                        'oper_station' => self::array_value($dislocation, 'oper_station'),
                        'oper_station_name' => self::array_value($dislocation, 'oper_station_name'),
                        'oper_code' => self::array_value($dislocation, 'oper_code'),
                        'oper_date' => self::array_value($dislocation, 'oper_date'),
                        'train_num' => self::array_value($dislocation, 'train_num'),
                        'dest_station' => self::array_value($dislocation, 'dest_station'),
                        "dest_station_name" => self::array_value($dislocation, 'dest_station_name'),
                        "rest_distance" => intval(self::array_value($dislocation, 'rest_distance')),
                        "loading_date" => self::array_value($dislocation, 'loading_date'),
                        "oper_full_name" => self::array_value($dislocation, 'oper_full_name')
                    ]
                );
                $Dislocation_model->save();
            }
    }

    /**
     * @return array|null
     */
    public static function getActiveTasks() {
        $Task_model = new \Models\Tasks();
        return $Task_model->where('removeUserId', NULL, 'is')->join('userCreate')->ArrayBuilder()->get();
    }

    /**
     * @param $res
     * @param $territory_id
     * @return bool
     */
    public static function addTasks($res, $territory_id) {

        if ($res===false || false === ($added_cars = self::array_value($res, 'added_cars')))
            return false;
        foreach ($added_cars as $added_car){
            $Task = new \Models\Tasks([
                'createUserId' => self::$User->id,
                'territory_id' => $territory_id,
                'car' => $added_car
            ]);
            $Task->save();
        }
        return true;

    }

    /**
     * @param string $str
     * @return int
     */
    public static function clearNumbers($str=''){
        return intval(preg_replace("/[^0-9]/", '', $str));
    }

    /**
     * @return mixed
     */
    public static function getUsers() {
        $User_model = new \Models\User();
        return $User_model->ArrayBuilder()->get();
    }

    /**
     * @param $login
     * @param $password
     * @param $firstName
     * @param $lastName
     * @return string
     * @throws Exception
     */
    public static function createUser($login, $password, $firstName, $lastName) {

        $password = self::validForm($password);
        $firstName = self::validForm($firstName);
        $lastName = self::validForm($lastName);

        $login = self::validFormLength(self::validForm($login), 'Логин');
        $password = self::validFormLength($password,'Пароль', 4, 40);
        $firstName = self::validFormLength($firstName, 'Имя');
        $lastName = self::validFormLength($lastName, 'Фамилия');

        if(empty(self::$messages)){
        $User_model = new \Models\User();
        if (!empty($User_model->where('login', $login)->getOne()))
            return self::showStatusMessage('Пользователь с таким логином существует');
        $obj = new \Models\User([
            'login' => $login,
            'active' => true,
            'firstName' => $firstName,
            'lastName' => $lastName,
            'createdAt' => (new \DB\DB())->now(),
            'password' => sha1($password)
        ]);

        $id  = $obj->save();
        return self::showStatusMessage('Пользователь создан', true);
        }
    }

    /**
     * @param int $id
     * @return string
     */
    public static function deleteUser($id=0){
        if ($id<=1)
            return self::showStatusMessage('Неьзя удалить главного пользователя');
        $User_model = new \Models\User();
        $user = $User_model->byId($id);
        if (empty($user))
            return self::showStatusMessage('Пользователя не существует');
        $user->delete();
        return self::showStatusMessage('Пользователь удален', true);
    }

    /**
     * @param $config_data
     * @return bool
     */
    public static function createConfig($config_data) {

        if (file_exists(__DIR__ . "/../config.php"))
            return true;

        if (false=== ($form = static::array_value($config_data, 'formPush')))
            return false;

        $parent = __DIR__ . '/../config.default.php';
        $child = __DIR__ . '/../config.php';

        copy($parent, $child);

        $file_contents = file_get_contents($child);

        $file_contents = str_replace('%DB_HOST%', $_POST['host_u'], $file_contents);
        $file_contents = str_replace('%DB_NAME%', $_POST['login_u'], $file_contents);
        $file_contents = str_replace('%DB_USERNAME%', $_POST['DB_USERNAME'], $file_contents);
        $file_contents = str_replace('%DB_PASS%', $_POST['pass_u'], $file_contents);
        $file_contents = str_replace('%VAGON_LOGIN%', $_POST['vagon_login'], $file_contents);
        $file_contents = str_replace('%VAGON_PASSWORD%', $_POST['vagon_password'], $file_contents);
        $file_contents = str_replace('%VAGON_TOKEN%', $_POST['vagon_token'], $file_contents);
        $file_contents = str_replace('%vagon_pref%', $_POST['vagon_pref'], $file_contents);

        file_put_contents($child, $file_contents);
        require_once __DIR__ . '/../config.php';
        return true;
    }
    public static function validForm($el){
        return htmlspecialchars(trim($el));
    }

    /**
     * @param $el
     * @param string $label
     * @param string|int $min
     * @param string|int $max
     * @return bool|string
     */
        public static function validFormLength($el, $label='', $min='', $max=''){
        if ($min === '') $min = 3;
        if ($max === '') $max = 40;
        $el = self::validForm($el);
        if(mb_strlen($el) < $min || mb_strlen($el) > $max)
            return self::showStatusMessage("Поле {$label} должно быть от {$min} до {$max} символов");
        else return $el;
    }




}