<?php
/**
 * Created by PhpStorm.
 * User: klish
 * Date: 06.09.2019
 * Time: 21:32
 */

class Url
{

    /**
     * @var resource
     */
    private $ch;

    /**
     * @var
     */
    private $info;

    /**
     * @var array
     */
    private $headers=[];

    /**
     * @var bool
     */
    private $response = false;

    /**
     * @var string
     */
    private $url;

    public function __construct($url, $url_parameters=[], $headers=[], $connect_timeout=30)
    {

        $this->ch = curl_init();

        $this->url = $url;

        $this->headers = $headers;

        if (!empty($url_parameters))
            $this->url .= "?" . http_build_query($url_parameters);

        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);

        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);

        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, $connect_timeout);




    }

    /**
     * @param array $data
     * @param string $response_type
     * @return bool|mixed
     */
    public function get($data=[], $response_type='json'){

        if (!empty($data))
            $this->url .= "?" . http_build_url($data);

        curl_setopt($this->ch, CURLOPT_HTTPGET, 1);

        return $this->query($response_type);

    }

    /**
     * @param $data
     * @param string $response_type
     * @return bool|mixed
     */
    public function post($data, $response_type='json') {

        curl_setopt($this->ch, CURLOPT_POST, 1);

        curl_setopt($this->ch, CURLOPT_POSTFIELDS, http_build_query($data));

        return $this->query($response_type);

    }

    /**
     * @param $data
     * @param string $response_type
     * @return bool|mixed
     */
    public function json($data, $response_type='json') {

        $json = json_encode($data);

        $this->headers[] = 'Content-Type: application/json';

        curl_setopt($this->ch, CURLOPT_POST, 1);

        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $json);

        return $this->query($response_type);

    }

    /**
     * @param $response_type
     * @return bool|mixed
     */
    private function query($response_type) {

        curl_setopt($this->ch, CURLOPT_URL, $this->url);

        if (!empty($this->headers))
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, $this->headers);

        $this->response = curl_exec($this->ch);

        $this->info = curl_getinfo($this->ch);

        curl_close($this->ch);

        if ($response_type === 'json')
            return json_decode($this->response, true);

        return $this->response;

    }

}