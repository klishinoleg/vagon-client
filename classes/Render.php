<?php
/**
 * Created by PhpStorm.
 * User: klish
 * Date: 08.09.2019
 * Time: 21:15
 */

class Render
{
    /**
     * @param $arr
     * @param bool|mixed $selected
     * @return string
     */
    public static function getOptionFromArray ($arr, $selected=false) {
        print_r($arr);
        $options=[];
        foreach ($arr as $value => $label) {
            $s = ($value===$selected) ? ' selected ' : '';
            $options[] = "<option value='{$value}' {$s} >{$label}</option>";
        }
        return implode("\n", $options);
    }

    /**
     * @param array $arr
     * @return string
     */
    public static function showMessages($arr=[]){
        $messages = [];
        foreach ($arr as $value_key => &$value)
            if (is_array($value))
                foreach ($value as $item_key => $item)
                    $messages[] = "{$item_key}: {$item}";
            else
                $messages[] = "{$value_key}: {$value}";
        return implode('<br />', $messages);
    }

}