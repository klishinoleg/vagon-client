<?php
/**
 * Created by PhpStorm.
 * User: klish
 * Date: 06.09.2019
 * Time: 20:18
 */

namespace System;


use DB\DB;
use DB\MysqliDb;

class Install
{
    /**
     * @var MysqliDb
     */
    protected $db;

    public function __construct()
    {
        $this->db = new DB();
    }

    /**
     *
     */
    public function createTables() {
        foreach ($this->getTables() as $name => $data)
            $this->createTable($name, $data);

    }

    /**
     *
     */
    public function dropTables() {
        foreach ($this->getTables() as $name => $data)
            $this->dropTable($name);
    }

    public function getFieldsForDislocations() {

        return [
            'createdAt' => 'datetime',
            'serviceId' => 'int(10)',
            'car' => 'int(10)',
            'oper_station' => 'varchar(50)',
            'oper_station_name' => 'varchar(50)',
            'oper_code' => 'varchar(50)',
            'oper_date' => 'datetime',
            'train_num' => 'int(10)',
            'dest_station' => 'varchar(50)',
            "dest_station_name" => 'varchar(50)',
            'rest_distance' => 'int(10)',
            'loading_date' => 'datetime',
            'oper_full_name' => 'varchar(50)'
        ];

    }

    public function  getFieldsForTasks() {
        return [
            'createUserId' => 'int(10)',
            'createdAt' => 'datetime',
            'removeUserId' => 'int(10)',
            'removeAt' => 'datetime',
            'territory_id' => 'int(10)',
            'car' => 'int(10)'
        ];
    }

    public function getFieldsForUsers(){

        return [

            'login' => 'char(30) not null',
            'active' => 'bool default 0',
            'customerId' => 'int(10) null',
            'firstName' => 'char(30) not null',
            'lastName' => 'char(30)',
            'password' => 'text not null',
            'createdAt' => 'datetime',
            'updatedAt' => 'datetime',
            'expires' => 'datetime',
            'loginCount' => 'int(10) default 0',
            'PHPSESSID' => 'char (50)'

        ];

    }

    /**
     * @return array
     */
    private function getTables(){
        $tables = [];
        foreach (get_class_methods($this) as $class_method)
            if (strpos($class_method, 'getFieldsFor') !== false)
                $tables[strtolower(str_replace('getFieldsFor', '', $class_method))] = $this->$class_method();
        return $tables;
    }

    /**
     * @param string $name
     * @param array $data
     * @throws \Exception
     */
    private function createTable ($name='', $data=[]) {

        if ($this->db->tableExists($name))
            return;

        $px = VAGON_PREFIX;

        $q = "CREATE TABLE IF NOT EXISTS {$px}{$name} (id INT(9) UNSIGNED PRIMARY KEY AUTO_INCREMENT";
        foreach ($data as $k => $v) {
            $q .= ", `{$k}` {$v}";
        }
        $q .= ")";
        $this->db->rawQuery($q);
    }

    /**
     * @param string $name
     */
    private function dropTable($name=''){
        $this->db->rawQuery("DROP TABLE `{$name}`");
    }



}