<?php
define('DB_HOST', '%DB_HOST%');
define('DB_NAME', '%DB_NAME%');
define('DB_USERNAME', '%DB_USERNAME%');
define('DB_PASSWORD', '%DB_PASS%');

define('VAGON_LOGIN', '%VAGON_LOGIN%');
define('VAGON_PASSWORD', '%VAGON_PASSWORD%');
define('VAGON_TOKEN', '%VAGON_TOKEN%');
define('VAGON_PREFIX', '%vagon_pref%' . '_');

/*
* https://github.com/ThingEngineer/PHP-MySQLi-Database-Class/blob/master/dbObject.md
* https://github.com/ThingEngineer/PHP-MySQLi-Database-Class/blob/master/tests/dbObjectTests.php
* https://github.com/ThingEngineer/PHP-MySQLi-Database-Class/blob/master/tests/models/user.php
 *
 */