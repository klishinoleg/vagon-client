<?php
session_start();
if (!defined('AUTH'))
    define('AUTH', true);

spl_autoload_register(function ($class_name){
    $dir = (strpos($class_name, 'Model') !== false) ? 'models' : 'classes';


    $file = __DIR__ . DIRECTORY_SEPARATOR . "{$dir}" . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $class_name).'.php';

    if (strpos($file, 'Models' . DIRECTORY_SEPARATOR))

        $file = str_replace('Models' . DIRECTORY_SEPARATOR, '', $file);
    if (!class_exists($class_name))
        try {
//
            require_once $file;
        }
        catch (Exception $e){
            echo $file, "\n";
            echo $e->getMessage(), "\n";
            exit();
        }
});

require_once __DIR__ . "/functions.php";

if (file_exists(__DIR__ . "/config.php")) {
    require_once __DIR__ . "/config.php";

if(true === AUTH && !isset($not_auth))
    if (false === Functions::Autorisation()) {
        require __DIR__ . "/view/view_auth.php";
        exit;
    }

if (Functions::array_value($_GET, 'exit') === 'Y'){
    Functions::userExit();
    Functions::redirect('/');
}

    try {
        if (true === AUTH) {
            (new \System\Install())->getFieldsForUsers();
        }
    } catch (Exception $e) {
        var_dump($e);
    }
}

//$data = json_decode('{"removed_cars":["74854688"]}', true);

//var_dump(Functions::removeTasks($data));

//var_dump(Functions::addTasks($data, '20'));

//try {
//    $db = new \DB\DB();
//    $obj = new \Models\User([
//        'login' => 'klishinoleg',
//        'active' => true,
//        'firstName' => 'Олег',
//        'lastName' => 'Клишин',
//        'password' => $db->func('SHA1(?)', Array("olegklishin")),
//        'expires' => $db->now('+1Y')
//    ]);
//    $id  = $obj->save();
//    if ($obj->errors) {
//        echo "errors:";
//        print_r ($obj->errors);
//        exit;
//    }
//}
//catch (Exception $exception) {
////    var_dump($exception->getMessage());
//}



