<?php
session_start();
require_once __DIR__ . "/loader.php";
$result = [];

if (Functions::array_value($_POST, 'add_task') === 'Y')
    $result[] = Functions::addDislocation(
                    Functions::array_value($_POST, 'territory_id'),
                    Functions::array_value($_POST, 'cars_id')
    );

elseif (!empty($remove_car = Functions::array_value($_GET, 'remove')))
    $result[] = Functions::removeDislocations($remove_car);
require __DIR__ . "/view/view_index.php";
?>

