<?php
session_start();
define('AUTH', false);
require_once __DIR__ . "/loader.php";

$create_config = Functions::createConfig($_POST);


if (file_exists(__DIR__ . '/config.php')) {
    $DB = (new DB\DB());
    try {
        $DB->ping();
        (new \System\Install())->createTables();

        if (true === $DB->tableExists(VAGON_PREFIX . 'users') && false === $DB->has(VAGON_PREFIX . 'users')) {

            require_once __DIR__ . "/users.php";
        }
        else {
            echo 'Done';
        }

    }
    catch (Exception $exception){
        var_dump($exception);
        echo 'No';
        unlink(__DIR__ . "/config.php");
    }
}
if  (!true === $create_config) {
    require __DIR__ . "/view/view_reg-main.php";
}