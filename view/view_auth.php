<?php

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../style.css">
    <title>Document</title>
</head>
<body>
    <div class="wrapper">
        <h1>Авторизация</h1>
        <form action="" method="POST">
            <label for="name">Логин</label>
            <input type="text" name="name_main" id="name" required>
            <label for="password">Пароль</label>
            <input type="password" name="password_main" id="password" required>
            <div class="results">
                <?= Functions::showMessagesLikeString();?>
            </div>
            <input type="submit" value="Войти" class="btn">
        </form>
    </div>
</body>
</html>




