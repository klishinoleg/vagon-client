<?php
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../style.css">
    <title>Личный кабинет</title>
    <!-- b0a8e2d8ccb04b24683d347076e80d29e451a385:d3aa2e6571e673001cb012eda23bd97d02234f0b -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.10/lodash.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans&amp;subset=cyrillic,latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Caption&amp;subset=cyrillic,latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/solid.css"
          integrity="sha384-wnAC7ln+XN0UKdcPvJvtqIH3jOjs9pnKnq9qX68ImXvOGz2JuFoEiCjT8jyZQX2z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/fontawesome.css"
          integrity="sha384-HbmWTHay9psM8qyzEKPc8odH4DsOuzdejtnr+OFtDmOcIVnhgReQ4GZBH7uwcjf6" crossorigin="anonymous">
</head>
<body>
<div class="wrapper_index">

    <div class="index_header">
        <div class="header">
           <h3> Добро пожаловать, <?=Functions::getUser()->firstName;?><br /></h3>
            <a class="a_btn" href="/?exit=Y">Выйти</a>
            <?if(Functions::getUser()->id == 1):?>
                <br /><a href="/users.php" class="a_btn">Управление пользователями</a>
            <?endif;?>
        </div>
        <?if(!empty($result)):?>
            <div class="results">
                <?=Render::showMessages($result);?>
            </div>
        <?endif;?>
    </div>
    <div class="tasks">
        <h1>Личный кабинет</h1>
        <?if(!empty($active_tasks = Functions::getActiveTasks())):?>
            <div class="tasks__list">
                <table>
                    <thead>
                    <tr>
                        <th>Автор</th>
                        <th>Дата создания</th>
                        <th>Территория</th>
                        <th>Вагон</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?foreach ($active_tasks as $active_task):?>
                        <tr>
                            <td title="<?=$active_task['firstName'];?> <?=$active_task['lastName'];?>"><?=$active_task['login'];?></td>
                            <td><?=$active_task['createdAt'];?></td>
                            <td><?=\Api\Vagon::TERRITORIES[$active_task['territory_id']];?></td>
                            <td><?=$active_task['car'];?></td>
                            <td><a href="/?remove=<?=$active_task['car'];?>">Снять</a></td>
                        </tr>
                    <?endforeach;?>
                    </tbody>
                </table>
            </div>
        <?endif;?>
        <div class="tasks__add">
            <h2>Добавить вагоны на слежение</h2>
            <form method="post">
                <input type="hidden" name="add_task" value="Y"/>
                <div class="input_group">
                    <label for="territory_id">ID территории</label>
                    <select id="territory_id" name="territory_id" type="text" required>
                        <?=Render::getOptionFromArray(\Api\Vagon::TERRITORIES, 20);?>
                    </select>

                </div>
                <div class="input_group">
                    <label for="cars_id">ID вагонов (можно через запятую)</label>
                    <input id="cars_id" name="cars_id" type="text" value=""  required>
                </div>
                <div class="results">
                    <?= Functions::showMessagesLikeString();?>
                </div>
                <div class="input_group">
                    <input type="submit" value="Добавить" required/ class='btn'>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
