<?php
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../style.css">
    <title>Document</title>
</head>
<body>
    <div class="wrapper">
        <h1>Регистрация</h1>
        <form method="POST" action="">
            <label for="host_u">Хост от БД</label>
            <input type="text"  name="host_u" id="host_u">
            <label for="DB_USERNAME">Название БД</label>
            <input type="text"  name="DB_USERNAME" id="DB_USERNAME">
            <label for="login_u">Логин для БД</label>
            <input type="text"  name="login_u" id="login_u">
            <label for="login_u">Пароль для БД</label>
            <input type="password"  name="pass_u"  id="login_u">
            <label for="vagon_pref">Выберите префикс БД</label>
            <input type="text"  name="vagon_pref"  id="vagon_pref">
            <label for="vagon_login">Логин для доступа к vagon.ru</label>
            <input type="text"  name="vagon_login"  id="vagon_login">
            <label for="vagon_password">Пароль для доступа к vagon.ru</label>
            <input type="password"  name="vagon_password"  id="vagon_password">
            <label for="vagon_token">Токен для доступа к vagon.ru</label>
            <input type="text"  name="vagon_token"  id="vagon_token">


            <input type="submit" name="formPush" value="Отправить" class="btn">
        </form>
    </div>
</body>
</html>