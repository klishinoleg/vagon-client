<?php
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../style.css">
    <title>Управление пользователями</title>
    <!-- b0a8e2d8ccb04b24683d347076e80d29e451a385:d3aa2e6571e673001cb012eda23bd97d02234f0b -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.10/lodash.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans&amp;subset=cyrillic,latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Caption&amp;subset=cyrillic,latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/solid.css"
          integrity="sha384-wnAC7ln+XN0UKdcPvJvtqIH3jOjs9pnKnq9qX68ImXvOGz2JuFoEiCjT8jyZQX2z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/fontawesome.css"
          integrity="sha384-HbmWTHay9psM8qyzEKPc8odH4DsOuzdejtnr+OFtDmOcIVnhgReQ4GZBH7uwcjf6" crossorigin="anonymous">

</head>
<body>
<div class="wrapper_lich">
    <aside class="add_users">
        <div class="header">
           <h3> Добро пожаловать  <?=Functions::getUser()->firstName;?><br /></h3>
            <div class="btn_block">
                <a class='a_btn' href="/?exit=Y">Выйти</a>
                <a class='a_btn' href="/">Задачи</a>
            </div>
        </div>
        <h2>Добавить пользователей</h2>
        <form method="post">
            <input type="hidden" name="add_user" value="Y"/>

            <div class="input_group">
                <label for="login">Логин</label>
                <input id="login" name="login" type="text" value="">
            </div>
            <div class="input_group">
                <label for="password">Пароль</label>
                <input id="password" name="password" type="password" value="">
            </div>
            <div class="input_group">
                <label for="firstName">Имя</label>
                <input id="firstName" name="firstName" type="text" value="">
            </div>
            <div class="input_group">
                <label for="lastName">Фамилия</label>
                <input id="lastName" name="lastName" type="text" value="">
            </div>
                <div class="results">
                <?= Functions::showMessagesLikeString();?>
                </div>
            <div class="input_group">
                <input type="submit" value="Добавить" required/ class='btn'>
            </div>
        </form>
    </aside>

    <div class="tasks">
        <h1>Пользователи</h1>
        <?if(!empty($users = Functions::getUsers())):?>
            <div class="tasks__list">
                <table>
                    <thead class="thead">
                    <tr>
                        <th>Логин</th>
                        <th>Имя</th>
                        <th>Фамилия</th>
                        <th>Регистрация</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?foreach ($users as $user):?>
                        <tr>
                            <td><?=$user['login'];?></td>
                            <td><?=$user['firstName'];?></td>
                            <td><?=$user['lastName'];?></td>
                            <td><?=$user['createdAt'];?></td>
                            <td><?if($user['id']>1):?><a class='red_btn' href="/users.php?remove=<?=$user['id'];?>">Удалить</a><?endif;?></td>
                        </tr>
                    <?endforeach;?>
                    </tbody>
                </table>
            </div>
        <?endif;?>
        <div class="tasks__add">

        </div>
    </div>
</div>
</body>
</html>